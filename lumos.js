document.addEventListener("DOMContentLoaded", (event) => {

  document.getElementById('audio').addEventListener('canplaythrough', () => {
    console.log('Ready to play audio');
    document.getElementById('episode').style.visibility = 'visible';
    document.body.style.backgroundImage = 'url(spinner.png)';
  });

  document.getElementById('year').addEventListener('change', () => {
    document.getElementById('form').submit();
  });

});

