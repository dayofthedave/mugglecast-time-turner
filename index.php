<?php
require 'app.php';
$that = new MuggleCastTimeTurner();
?>
<html>
<head>
  <title>MuggleCast Time Turner</title>
  <link rel="shortcut icon" href="icon.png">
  <meta name="viewport" content="initial-scale=1, width=device-width">
  <link rel="stylesheet" href="lumos.css">
  <script src="lumos.js"></script>
  <?php foreach ($that->log as $msg): ?><script>console.log(<?=$msg?>)</script><?php endforeach; ?>
</head>
<body>
<main>
  <h1>MuggleCast Time Turner</h1>
  <section id="episode">
    <h2><?=$that->episode['title']?></h2>
    <p><?=$that->episode['date']?></p>
    <audio id="audio" src="<?=$that->episode['audio']?>#t=<?=$that->time?>" controls autoplay></audio>
    <form id="form" action="?" method="get">
      <select id="year" name="year">
        <option></option>
        <?php foreach ($that->years as $year => $index): ?>
          <option <?=$year==$that->year?'selected':''?>><?=$year?></option>
        <?php endforeach; ?>
      </select>
    </form>
  </section>
  <img src="spinner.png">
</main>
</body>
</html>