<?php

class MuggleCastTimeTurner {

  public $debug = false;
  public $log = array();

  public $episode;
  public $year;
  public $time;
  public $play;

  public $years = array(
    2005 => 0,
    2006 => 22,
    2007 => 71,
    2008 => 127,
    2009 => 168,
    2010 => 189,
    2011 => 218,
    2012 => 248,
    2013 => 261,
    2014 => 272,
    2015 => 276,
    2016 => 288,
    2017 => 309,
    2018 => 351,
    2019 => 400,
    2020 => 450,
    2021 => 499,
  );

  function __construct() {

    $this->episode = intval($_GET['episode']) ?? 0;
    $this->year = intval($_GET['year']) ?? 0;

    // Get all episodes from RSS feed
    $episodes = $this->get_all_episodes();

    // Is user requesting a specific episode?
    if ($this->episode > 0) {
      $episode = $this->episode_number();
      $this->episode = $episodes[$episode] ?? 0;
    }

    // Should we limit episodes to a particular year?
    elseif ($this->year > 0 && isset($this->years[$this->year])) {
      $offset = $this->years[$this->year];
      $length = ($this->years[$this->year+1] ?? count($episodes)) - $this->years[$this->year];
      $episodes = array_slice($episodes, $offset, $length);
      $this->episode = $episodes[rand(0, count($episodes))];
    }

    // Default to all episodes, excluding the most recent 100
    else {
      $this->episode = $episodes[rand(0, count($episodes) - 100)];
    }

    // Should we jump to a specified time or a random time?
    $this->time = is_numeric($_GET['time']) ? $_GET['time'] : rand(100, $this->episode['duration'] - 100);

    $this->console_log('episodes', $episodes);
    $this->console_log('episode', $this->episode);
    $this->console_log('time', $this->time);

  }

  public function get_all_episodes() {

    $xml = file_get_contents('https://audioboom.com/channels/4855957.rss');

    $dom = new DOMDocument();
    $dom->loadXML($xml);
    $tags = $dom->getElementsByTagName('item');

    $all = array();
    foreach($tags as $tag) {
      $date = date_format(date_create($tag->getElementsByTagName('pubDate')->item(0)->nodeValue), 'l, F d, Y');
      $title = $tag->getElementsByTagName('title')->item(0)->nodeValue;
      $audio = $tag->getElementsByTagName('content')->item(0)->getAttribute('url');
      $duration = $tag->getElementsByTagName('content')->item(0)->getAttribute('duration');
      $all[] = array('date' => $date, 'title' => $title, 'audio' => $audio, 'duration' => $duration);
    }

    // Sort episodes oldest to newest
    return array_reverse($all);
  }

  public function episode_number() {
    // 0 => 1: Episode 1
    // 1 => Welcome to MuggleCast
    // 2 => 2: Episode 2 (index/number equal)
    if ($this->episode <= 1) {
      $this->episode = 0;
    }
    // 281 => MuggleCast: The First Episode (index/number shifted by 1)
    if ($this->episode > 281) {
      $this->console_log("Episode > 281, shifting index");
      $this->episode++;
    }
    // 426 => 14: MuggleMiniCast #1 (index/number shifted by 2)
    if ($this->episode > 426) {
      $this->console_log("Episode > 426, shifting index again");
      $this->episode++;
    }
    // 450 => Why J.K. Rowling's Tweet Was Transphobic and Hurtful  (index/number shifted by 3)
    if ($this->episode > 450) {
      $this->console_log("Episode > 450, shifting index again");
      $this->episode++;
    }
    return $this->episode;
  }

  public function console_log($arg1, $arg2 = null) {
    if ($this->debug) {
      $this->log[] = $arg2 ? json_encode(array($arg1 => $arg2)) : json_encode($arg1);
    }
  }

}
