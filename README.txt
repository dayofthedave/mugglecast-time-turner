To install on mugglecast.com...

1) Create a folder named "time-turner" in the root WordPress directory, 
   alongside wp-admin, wp-content and wp-includes

2) Upload all these files to the "time-turner" folder:
    - app.php
    - icon.png
    - index.php
    - lumos.css
    - lumos.js
    - lumos.otf
    - spinner.gif
    - spinner.png

The app should now be online at https://mugglecast.com/time-turner
